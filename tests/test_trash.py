import os
import unittest
import shutil
import tempfile
from smartrm.parse_cmd_args import ParseCmdArgs
from smartrm.config import Config
from smartrm.trash_control import TrashControl
from smartrm.custom_exceptions import *


class TrashTest(unittest.TestCase):

    def setUp(self):
        self.args = ParseCmdArgs.parse(['rem', '--force'])
        self.config = Config()
        self.config.set_from_args(self.args)
        self.contr = TrashControl(self.args, self.config)
        self.trash_action = self.contr.trash_action

        self.trash = self.contr.trash
        self.trash.tr_dir = tempfile.mkdtemp()
        self.trash.check_create()
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

    def test_restore(self):
        os.mkdir('dir')
        self.trash.remove('dir')
        self.assertFalse(os.path.exists('dir'))
        self.trash_action.restore('dir')
        self.assertTrue(os.path.exists('dir'))
        self.assertFalse(os.path.exists(os.path.join(self.trash.tr_dir, 'files', 'dir')))
        self.assertFalse(os.path.exists(os.path.join(self.trash.tr_dir, 'info', 'dir.trashinfo')))

    def test_restore_damaged_error(self):
        os.mkdir('dir')
        self.trash.remove('dir')
        self.assertFalse(os.path.exists('dir'))
        self.trash.remove(self.trash.tr_dir+'/info/dir.trashinfo')
        self.assertRaises(CannotRestoreError, self.trash_action.restore, 'dir')

    def test_restore_nofile_error(self):
        self.assertRaises(NoFileInTrashError, self.trash_action.restore, 'file')

    def test_restore_nofile_dryrun(self):
        os.mkdir('dir')
        self.trash.remove('dir')
        self.trash_action.dryrun = True
        self.trash_action.restore('dir')
        self.assertFalse(os.path.exists('dir'))
        self.assertTrue(os.path.exists(os.path.join(self.trash.tr_dir, 'files', 'dir')))
        self.assertTrue(os.path.exists(os.path.join(self.trash.tr_dir, 'info', 'dir.trashinfo')))

    def test_clean(self):
        os.mkdir('dir')
        open('file', 'w').close()
        self.trash.remove('dir')
        self.trash.remove('file')
        self.trash_action.clean_trash()
        self.assertEqual(len(self.trash.get_files()), 0)

    def test_clean_dryrun(self):
        os.mkdir('dir')
        open('file', 'w').close()
        self.trash.remove('dir')
        self.trash.remove('file')
        self.trash_action.dryrun = True
        self.trash_action.clean_trash()
        self.assertEqual(self.trash.files_count(), 2)

    def test_del_from_trash(self):
        open('file', 'w').close()
        self.trash.remove('file')
        self.trash_action.del_from_trash('file')
        self.assertFalse(os.path.exists(self.trash.tr_dir+'/info/file.trashinfo'))
        self.assertFalse(os.path.exists(self.trash.tr_dir+'/files/file'))

    def test_del_from_trash_error(self):
        self.assertRaises(NoFileInTrashError, self.trash_action.del_from_trash, 'file')

    def test_del_from_trash_dryrun(self):
        open('file', 'w').close()
        self.trash.remove('file')
        self.trash_action.dryrun = True
        self.trash_action.del_from_trash('file')
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/info/file.trashinfo'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file'))

    def test_rm_older_than(self):
        open('file', 'w').close()
        self.trash.remove('file')
        self.trash.rm_older_than(0)
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/info/file.trashinfo'))
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))

    def test_rm_bigger_than(self):
        f = open('file', 'w')
        for i in range(100000):
            f.write('...............')
        f.close()
        open('file2', 'w')
        self.trash.remove('file')
        self.trash.remove('file2')
        self.trash.rm_bigger_than(1)
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def tearDown(self):
        os.chdir('..')
        shutil.rmtree(self.trash.tr_dir)
        shutil.rmtree(self.temp_dir)


if __name__ == '__main__':
    unittest.main()
