import os
import unittest
import shutil
import tempfile
from smartrm.parse_cmd_args import ParseCmdArgs
from smartrm.remove_control import RemoveControl
from smartrm.config import Config
from smartrm.custom_exceptions import *
from smartrm.trash import Trash


class RmTest(unittest.TestCase):

    def setUp(self):
        self.args = ParseCmdArgs.parse(['rem', '--force'])
        self.config = Config()
        self.config.set_from_args(self.args)
        self.contr = RemoveControl(self.args, self.config)
        self.remover = self.contr.remover
        self.trash = self.contr.trash

        self.trash.tr_dir = tempfile.mkdtemp()
        self.trash.check_create()
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

    def test_rm_nonrecurs_file(self):
        os.mkdir('dir')
        self.remover.rm_nonrecurs('dir')
        self.assertFalse(os.path.exists('dir'))

    def test_rm_nonrecurs_dir(self):
        open('file', 'w').close()
        self.remover.rm_nonrecurs('file')
        self.assertFalse(os.path.exists('file'))

    def test_rm_nonrecurs_error(self):
        os.mkdir('dir')
        open('dir/file', 'w').close()
        self.assertRaises(NotEmptyDirError, self.remover.rm_nonrecurs, 'dir')

    def test_rm_dir_tree(self):
        os.mkdir('dir')
        open('dir/file1', 'w').close()
        os.mkdir('dir/dir1')
        self.remover.rm_dir_tree('dir')
        self.assertFalse(os.path.exists('dir'))

    def test_rm_file(self):
        open('file', 'w').close()
        self.remover.rm_file('file')
        self.assertFalse(os.path.exists('file'))

    def test_rm_file_error(self):
        os.mkdir('dir')
        self.assertRaises(DirectoryRmError, self.remover.rm_file, 'dir')

    def test_rm_regex(self):
        os.mkdir('reg')
        open('reg/f1', 'w').close()
        open('reg/f2', 'w').close()
        open('reg/fb', 'w').close()

        self.remover.rm_regex('reg', 'f+\d')
        self.assertFalse(os.path.exists('reg/f1'))
        self.assertFalse(os.path.exists('reg/f2'))
        self.assertTrue(os.path.exists('reg/fb'))

    def test_rm_recursive(self):
        os.mkdir('dir')
        open('dir/f1', 'w').close()
        os.mkdir('dir/dir1')
        open('dir/dir1/f2', 'w').close()
        self.remover.rm_recursive('dir')
        self.assertFalse(os.path.exists('dir'))

    def test_rm_notrash(self):
        os.mkdir('dir')
        n1 = self.trash.files_count()
        self.remover.rm_notrash('dir')
        n2 = self.trash.files_count()
        self.assertEqual(n1, n2)
        self.assertFalse(os.path.exists('tfld'))

    def test_duplicate_check(self):
        open('f', 'w').close()
        open('f.1', 'w').close()
        n = self.trash.duplicate_check(os.getcwd(), 'f')
        self.assertEqual(n, 2)

    def test_dryrun(self):
        os.mkdir('dir')
        self.remover.dryrun = True
        self.remover.remove('dir')
        self.remover.dryrun = False
        self.assertTrue(os.path.exists('dir'))

    def test_remove(self):
        os.mkdir('dir')
        self.remover.remove('dir')
        self.assertFalse(os.path.exists('dir'))
        self.assertTrue(os.path.exists((self.trash.tr_dir+'/files/dir')))
        self.assertTrue(os.path.exists((self.trash.tr_dir+'/info/dir.trashinfo')))

    def tearDown(self):
        os.chdir('..')
        shutil.rmtree(self.trash.tr_dir)
        shutil.rmtree(self.temp_dir)

if __name__ == '__main__':
    unittest.main()
