import os
import unittest
import shutil
from smartrm.parse_cmd_args import ParseCmdArgs
from smartrm.remove_control import RemoveControl
from smartrm.config import Config
from smartrm.clean_politics import CleanPolitics
from smartrm.restore_politics import RestorePolitics


class PoliticsTest(unittest.TestCase):

    def setUp(self):
        self.args = ParseCmdArgs.parse(['rem', '--force'])
        self.config = Config()
        self.config.set_from_args(self.args)
        self.contr = RemoveControl(self.args, self.config)
        self.remover = self.contr.remover
        self.trash = self.contr.trash
        self.trash.clean_trash()
        os.mkdir('smrmtest__')
        os.chdir('smrmtest__')
        self.config.rem_size = 1
        f = open('file', 'w')
        for i in range(100000):
            f.write('...............')
        f.close()
        open('file2', 'w').close()
        self.remover.remove('file')
        self.remover.remove('file2')
        open('file', 'w').close()

    # clean politics tests
    def test_clean_always(self):
        self.config.clean_politics = 'always'
        clean = CleanPolitics(self.config, self.trash)
        clean.run()
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_optional_true(self):
        self.config.max_tr_size = 1
        self.config.clean_politics = 'optional'
        clean = CleanPolitics(self.config, self.trash)
        clean.run()
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_optional_false(self):
        self.config.max_tr_size = 1000
        self.config.clean_politics = 'optional'
        clean = CleanPolitics(self.config, self.trash)
        clean.run()
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_clean_never(self):
        self.config.clean_politics = 'never'
        clean = CleanPolitics(self.config, self.trash)
        clean.run()
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))


    # restore politcs tests
    def test_replace(self):
        polit = RestorePolitics('replace', self.trash.tr_dir)
        polit.run('file', os.path.curdir + '/file')
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(os.path.curdir + '/file'))

    def test_save_both(self):
        polit = RestorePolitics('saveboth', self.trash.tr_dir)
        polit.run('file', os.path.curdir + '/file')
        self.assertTrue(os.path.exists(os.path.curdir + '/file'))
        self.assertTrue(os.path.exists(os.path.curdir + '/file(1)'))

    def tearDown(self):
        os.chdir('..')
        shutil.rmtree('smrmtest__')
        self.trash.clean_trash()

if __name__ == '__main__':
    unittest.main()
