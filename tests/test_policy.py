import os
import unittest
import shutil
import tempfile
from smartrm.parse_cmd_args import ParseCmdArgs
from smartrm.remove_control import RemoveControl
from smartrm.config import Config
from smartrm.clean_policy import CleanPolicy
from smartrm.restore_policy import RestorePolicy


class PolicyTest(unittest.TestCase):

    def setUp(self):
        self.args = ParseCmdArgs.parse(['rem', '--force'])
        self.config = Config()
        self.config.set_from_args(self.args)
        self.contr = RemoveControl(self.args, self.config)
        self.remover = self.contr.remover
        self.trash = self.contr.trash

        self.trash.tr_dir = tempfile.mkdtemp()
        self.trash.check_create()
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

        self.config.rem_size = 1
        with open('file', 'w') as f:
            for i in range(100000):
                f.write('...............')

        open('file2', 'w').close()
        self.trash.remove('file')
        self.trash.remove('file2')
        open('file', 'w').close()

    # clean policy tests
    def test_clean_always(self):
        self.config.clean_policy = 'always'
        clean = CleanPolicy(self.config, self.trash)
        clean.run()
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_optional_true(self):
        self.config.max_tr_size = 1
        self.config.clean_policy = 'optional'
        clean = CleanPolicy(self.config, self.trash)
        clean.run()
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_optional_false(self):
        self.config.max_tr_size = 1000
        self.config.clean_policy = 'optional'
        clean = CleanPolicy(self.config, self.trash)
        clean.run()
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    def test_clean_never(self):
        self.config.clean_policy = 'never'
        clean = CleanPolicy(self.config, self.trash)
        clean.run()
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(self.trash.tr_dir + '/files/file2'))

    # restore policy tests
    def test_replace(self):
        self.trash.restore('file', RestorePolicy('replace'))
        self.assertFalse(os.path.exists(self.trash.tr_dir + '/files/file'))
        self.assertTrue(os.path.exists(os.path.curdir + '/file'))

    def test_save_both(self):
        self.trash.restore('file', RestorePolicy('saveboth'))
        self.assertTrue(os.path.exists(os.path.curdir + '/file'))
        self.assertTrue(os.path.exists(os.path.curdir + '/file(1)'))

    def tearDown(self):
        os.chdir('..')
        shutil.rmtree(self.trash.tr_dir)
        shutil.rmtree(self.temp_dir)

if __name__ == '__main__':
    unittest.main()
