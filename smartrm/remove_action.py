import os
import re
import datetime
from custom_exceptions import *


def dryrun(func):
    def inner(*args):
        self = args[0]
        if not self.dryrun:
            func(*args)
        else:
            self.report.info("--dry-run--")
            self.report.removed(args[1])
    return inner


class RemoveAction(object):

    """
    Class for remove methods
    Args:
        dry-run: boolean value for
        report: Report object for intetaction with user
        no_trash: boolean value for
        tr_dir: trash directory
    Uses dryrun decorator
    """

    def __init__(self, trash, report, confirm, dry_run, no_trash):
        self.trash = trash
        self.report = report
        self.confirm = confirm
        self.notrash = no_trash
        self.dryrun = dry_run

    def rm_nonrecurs(self, path):
        """
        Remove file or empty directory
        :param path:  file or dir to remove
        """
        if os.path.isdir(path):
            if os.listdir(path).__len__() == 0:
                if self.confirm.prompt("remove empty directory '/{}'?".format(path)):
                    self.remove(path)
            else:
                raise NotEmptyDirError(path)

        else:
            if self.confirm.prompt("remove file '/{}'?".format(path)):
                self.remove(path)

    def rm_recursive(self, path):
        """
        Recursive directory remove with files in it
        :param path:  dir to remove
        """
        if os.path.isdir(path):
            if os.listdir(path).__len__() != 0:
                if self.confirm.prompt("go into directory '/{}'?".format(path)):
                    for file_ in os.listdir(path):
                        self.rm_recursive(os.path.join(path, file_))

            if os.listdir(path).__len__() == 0:
                if self.confirm.prompt("remove empty directory '/{}'?".format(path)):
                    self.remove(path)

        else:
            if self.confirm.prompt("remove file '/{}'?".format(path)):
                self.remove(path)

    def rm_dir_tree(self, path):
        """
        Removes a directory tree
        :param path: directory to remove
        """
        self.remove(path)

    def rm_file(self, path):
        """
        Removes a file. If directory is given throws exeption
        :param path: directory to remove
        """
        if os.path.isdir(path):
            raise DirectoryRmError(path)
        else:
            self.remove(path)

    def rm_regex(self, path, regex):
        """
        Searches recursively and removes files matching
        a regex in given directory
        :param path: dir to search regex files
        :param regex: regex string
        """
        if os.path.isdir(path) and os.listdir(path).__len__() != 0:
            for file_ in os.listdir(path):
                self.rm_regex(os.path.join(path, file_), regex)

        else:
            r = re.search(regex, path)
            if r is not None:
                if self.confirm.prompt("remove file '/{}'?".format(path)):
                    self.remove(path)

    @dryrun
    def rm_notrash(self, path):
        if os.path.isdir(path):
            os.rmdir(path)
        else:
            os.remove(path)
        self.report.removed(path)

    @dryrun
    def remove(self, path):
        if not os.access(path, os.W_OK):
            if not self.confirm.prompt('file is protected. do you want to delete it?'):
                return
        if self.notrash:
            self.rm_notrash(path)
            return

        self.trash.remove(path)
        self.report.removed(path)

