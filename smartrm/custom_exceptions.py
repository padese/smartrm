class AccessError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "cannot delete '{}': access error".format(self.value)


class DirectoryRmError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "cannot delete directory '{}' (use -d / -r / -t)".format(self.value)


class NoFileError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "file or directory '{}' not found".format(self.value)


class NotEmptyDirError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "cannot delete: '/{}' not empty".format(self.value)


class NoFileInTrashError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "file or directory {} doesn't exist in trash".format(self.value)


class CannotRestoreError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "cannot restore {}. file is damaged".format(self.value)


class NoConfigFileError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "config file not found".format(self.value)
