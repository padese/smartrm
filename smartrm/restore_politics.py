import os
import shutil

class RestorePolitics(object):

    """
    Used when the restoring file conflicts with an existing file
        polit: restore politics:
            replace: replace existing file
            saveboth: save restoring file with '(\d)' adding
            ask: ask user to choose action
        tr_dir: trash directory
    """

    def __init__(self, polit, tr_dir):
        self.dict = {'replace': self.restore_replace, 'saveboth': self.restore_save_both,
                     'ask': self.restore_ask}
        self.func = self.dict[polit]
        self.tr_dir = tr_dir

    def run(self, *args):
        self.func(*args)

    def restore_replace(self, file_, path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
        os.rename(os.path.join(self.tr_dir, 'files', file_), path)
        os.remove(os.path.join(self.tr_dir, 'info', file_ + '.trashinfo'))

    def restore_save_both(self, file_, path):
        ind = 1
        while os.path.exists(path + '.{}'.format(ind)):
            ind += 1
        path += '({})'.format(ind)
        os.rename(os.path.join(self.tr_dir, 'files', file_), path)
        os.remove(os.path.join(self.tr_dir, 'info', file_ + '.trashinfo'))

    def restore_ask(self, file_, path):
        print 'file {} exists. choose action: [r]eplace, save [b]oth, [n]ot restore'.format(file_)
        ch = raw_input()
        if ch == 'r':
            self.restore_replace(file_, path)
        elif ch == 'b':
            self.restore_save_both(file_, path)

