#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import sys

from remove_control import RemoveControl
from custom_exceptions import NoConfigFileError
from trash_control import TrashControl
from config import Config
from parse_cmd_args import ParseCmdArgs
from confirm import Confirm


def main():
    args = ParseCmdArgs.parse(sys.argv[1:])
    config = Config()
    try:
        config.fromjson()
    except NoConfigFileError:
        if Confirm.prompt_config():
            config.tojson()

    config.set_from_args(args)

    if args.command == 'rem':
        contr = RemoveControl(args, config)
        contr.run()
    elif args.command == 'trash':
        tr = TrashControl(args, config)
        tr.run()
    elif args.command == 'config':
        config.tojson()


if __name__ == "__main__":
    main()
