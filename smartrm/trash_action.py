import os
from restore_policy import RestorePolicy
from custom_exceptions import *
from datetime import datetime, timedelta


class TrashAction(object):

    """
    Class for actions with trash
    Args:
        report: Report object for intetaction with user
        config: smartrm configs
    """

    def __init__(self, trash, config, report):
        self.trash = trash
        self.report = report
        self.dryrun = config.dryrun
        self.restore_policy = RestorePolicy(config.restore_policy)

    def view_trash(self):
        """
        Prints files in trash,
        number of files and size
        """
        files = self.trash.get_files()
        str = 'Trash at {}:'.format(self.trash.tr_dir)
        str += '\n***\n'
        for f in files:
            str += f + '\n'
        str += '***\n'
        str += len(files).__str__() + ' file(s)\n'
        str += self.trash.get_size().__str__() + ' MBs'
        self.report.info(str)

    def restore(self, file_):
        """
        Restore file or directory from trash to previous location
        :param file_: file or dir to restore
        """
        if not os.path.exists(os.path.join(self.trash.tr_dir,'files', file_)):
            raise NoFileInTrashError(file_)
        if not os.path.exists(os.path.join(self.trash.tr_dir,'info', file_ + '.trashinfo')):
            raise CannotRestoreError(file_)

        if self.dryrun:
            self.report.info("--dry-run--")
        else:
            self.trash.restore(file_)

        self.report.restored(file_)

    def clean_trash(self):
        """
        Deletes all files from trash
        """
        if self.dryrun:
            self.report.info("--dry-run--")
        else:
            self.trash.clean()
        self.report.info('trash cleaned')

    def del_from_trash(self, file_):
        """
          Delete from trash permanently
          :param file_: file or dir to delete
          """
        if not os.path.exists(os.path.join(self.trash.tr_dir, 'files', file_)):
            raise NoFileInTrashError(file_)
        if self.dryrun:
            self.report.info("--dry-run--")
        else:
            self.trash.delete(file_)
        self.report.deleted(file_)




