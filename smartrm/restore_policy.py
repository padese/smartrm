import os
import shutil


class RestorePolicy(object):

    """
    Used when the restoring file conflicts with an existing file
        policy: restore policy:
            replace: replace existing file
            saveboth: save restoring file with '(\d)' adding
            ask: ask user to choose action
        tr_dir: trash directory
    """

    def __init__(self, policy):
        self.dict = {'replace': self.restore_replace, 'saveboth': self.restore_save_both,
                     'ask': self.restore_ask}
        self.func = self.dict[policy]

    def run(self, *args):
        return self.func(*args)

    def restore_replace(self, path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
        return path

    def restore_save_both(self, path):
        ind = 1
        while os.path.exists(path + '.{}'.format(ind)):
            ind += 1
        path += '({})'.format(ind)
        print path
        return path

    def restore_ask(self, file_, path):
        print 'file {} exists. choose action: [r]eplace, save [b]oth, [n]ot restore'.format(file_)
        ch = raw_input()
        if ch == 'r':
            path = self.restore_replace(path)
        elif ch == 'b':
            path = self.restore_save_both(path)
        return path
