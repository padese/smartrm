import logging
import os


class Report(object):

    """
    Class for logging
        args: command line arguments
    """

    def __init__(self, report, force):

        self.force = force

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(os.path.expanduser('~/smrm.log'))

        ch = logging.StreamHandler()
        if report:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        if force:
            ch.setLevel(logging.CRITICAL + 1)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        formatter = logging.Formatter('%(message)s')
        ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def removed(self, path):
        self.logger.debug('removed ' + os.path.abspath(path))

    def restored(self, path):
        self.logger.debug('restored ' + os.path.abspath(path))

    def deleted(self, path):
        self.logger.debug('deleted ' + os.path.abspath(path))

    def error(self, mes):
        self.logger.error(mes)

    def info(self, mes):
        self.logger.info(mes)
