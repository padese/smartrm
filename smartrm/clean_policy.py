class CleanPolicy(object):

    """
    Clean policy for trash:
        always: run every time
        optional: run if there is a few free space in trash
    Uses two methods to clean:
        rm old files (if config.rem_period is not 0)
        rm big files (if config.rem_size is not 0)
    args:
        config: smartrm configs
        trash: Trash object
    """

    def __init__(self, config, trash):
        self.dict = {'always': self.clean_always, 'optional': self.clean_optional, 'never': self.clean_never}
        self.func = self.dict[config.clean_policy]
        self.config = config
        self.trash = trash

    def run(self):
        self.func()

    def clean_always(self):
        if self.config.rem_period is not None and self.config.rem_period > 0:
            self.rm_older_than(self.config.rem_period)
        if self.config.rem_size is not None and self.config.rem_size > 0:
            self.rm_bigger_than(self.config.rem_size)

    def clean_optional(self):
        if self.config.max_tr_size - self.trash.getsize() < self.config.max_tr_size * 0.3:
            self.clean_always()

    def clean_never(self):
        pass

    def rm_bigger_than(self, max_size):
        self.trash.rm_bigger_than(max_size)

    def rm_older_than(self, days):
        self.trash.rm_older_than(days)
