import os
import sys
from remove_action import RemoveAction
from confirm import Confirm
from report import Report
from trash import Trash
from clean_policy import CleanPolicy
from custom_exceptions import *


class RemoveControl(object):

    """
    Class for pre-remove methods (trash auto-clean, validating arguments)
    and calling Remove methods
        args: command line arguments
        config: smartrm configs
    """

    def __init__(self, args, config):
        self.config = config
        self.args = args
        self.report = Report(config.report, config.force)
        self.confirm = Confirm(config.confirm)

        self.trash = Trash(self.config.tr_dir)

        self.remover = RemoveAction(self.trash, self.report, self.confirm, config.dryrun, config.notrash)
        self.clean_policy = CleanPolicy(self.config, self.trash)

    def run(self):
        """
        Validates cmd arguments and
        calls remove methods dependig on args
        """

        if not self.config.notrash:
            self.trash.check_create()

        self.clean_policy.run()

        if self.check_args_size() == 1:
            return

        if self.args.I:
            if not self.confirm.prompt_before_rm():
                return

        if self.args.regex is not None:
            remove = lambda f: self.remover.rm_regex(f, self.args.regex)
        elif self.args.r:
            remove = self.remover.rm_recursive
        elif self.args.t:
            remove = self.remover.rm_dir_tree
        elif self.args.d:
            remove = self.remover.rm_nonrecurs
        else:
            remove = self.remover.rm_file

        error_exit = False
        for file_ in self.args.files:
            if not os.path.exists(file_):
                self.report.error("file or directory '{}' not found".format(file_))
                error_exit = True
                continue

            try:
                remove(file_)
            except (NoFileError, AccessError, NotEmptyDirError, DirectoryRmError) as e:
                self.report.error(e)
                error_exit = True
        if error_exit:
            sys.exit(1)

    def check_args_size(self):
        """
        Size check of arguments
        If the size of all given files if bigger than trash free space,
        offers to rm files with no trash. If refused, doesn't rm anything
        """
        if self.config.notrash:
            return
        files_size = 0
        if files_size > self.config.max_tr_size - self.trash.get_size():
            if self.confirm.prompt("size of files is too big to rm to trash. rm without trash?"):
                self.config.notrash = True
            else:
                return 1
