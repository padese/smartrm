#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='smartrm',
      version='1.0',
      author='Denis Pastushik',
      author_email='denispastushik@gmail.com',
      packages=find_packages(),
      entry_points={'console_scripts': ['smrm = smartrm.smrm:main']}
     )